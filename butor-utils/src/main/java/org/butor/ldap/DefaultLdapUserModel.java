/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.ldap;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.PartialResultException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

public class DefaultLdapUserModel implements LdapUserModel {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	protected String ldapUrl = null;
	protected String domain = null;
	protected String searchBase = null;

	protected String adminUsername = null;
	protected String adminPwd = null;

	public DefaultLdapUserModel(String ldapUrl, String domain) {
		super();
		this.ldapUrl = ldapUrl;
		this.domain = domain;
	}

	protected DirContext getContext(String principal, String pwd) throws NamingException {
		// Set up the environment for creating the initial context
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

		env.put(Context.PROVIDER_URL, ldapUrl);
		if (ldapUrl.toLowerCase().startsWith("ldaps")) {
			env.put(Context.SECURITY_PROTOCOL, "ssl");
			env.put("java.naming.ldap.factory.socket", NoCheckSocketFactory.class.getName());
		}
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, principal);
		env.put(Context.SECURITY_CREDENTIALS, pwd);

		// Create context
		return new InitialDirContext(env);
	}

	protected void releaseContext(DirContext ctx) {
		if (ctx != null) {
			try {
				logger.debug("Releasing context.");
				ctx.close();
			} catch (Exception e) {
				logger.error("Error releasing context.", e);
			}
		}
	}

	@Override
	public LdapUser auth(String username, String pwd) {
		logger.info("Authenticating user {} in ldap {} ...", username, ldapUrl);
		DirContext ctx = null;
		long t = System.currentTimeMillis();
		try {
			String id = username;
			String email = username;
			int pos = id.indexOf("@");
			if (pos == -1)
				email += "@" +domain;
			else
				id = id.substring(0,pos);

			ctx = getContext(email, pwd);
			if (ctx == null)
				return null;

			// http://docs.oracle.com/cd/E26217_01/E35769/html/ldap-filters-attrs-users.html#ldap-filters-attributes-users-default
			String searchFilter = "(&(objectClass=user)(sAMAccountName=" + id + "))";
			StringWriter swDomain = new StringWriter();
			String[] domainSplit = domain.split("\\.");
			for (int i = 0; i < domainSplit.length; i++) {
				swDomain.append(String.format("DC=%s", domainSplit[i]));
				if (i + 1 < domainSplit.length)
					swDomain.append(",");
			}
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			NamingEnumeration<SearchResult> results = ctx.search(swDomain.toString(), searchFilter, searchControls);

			LdapUser user = null;
			while (results.hasMoreElements()) {
				SearchResult sr = (SearchResult) results.next();
				Attributes attrs = sr.getAttributes();
				user = retrieveUser(attrs, true);
				break;
			}
			return user;
		} catch (AuthenticationException e) {
			// Never mind this.
		} catch (NamingException e) {
			throw new RuntimeException(e);
		} finally {
			logger.info("Authentication done in ldap {}. Took {} ms", ldapUrl, System.currentTimeMillis() -t);
			releaseContext(ctx);
		}
		return null;
	}

	private LdapUser retrieveUser(Attributes attrs, boolean noNulls) throws NamingException {
/* debug
		NamingEnumeration<? extends Attribute> attributes = attrs.getAll();
		while (attributes.hasMoreElements()) {
			Attribute attr = attributes.next();
			System.out.println(attr.getID() + ": " + attr.get());
		}
		System.out.println("======================");
*/
		Object sn = attrs.get("sn");
		Object givenName = attrs.get("givenName");
		Object mail = attrs.get("mail");
		Object phone = attrs.get("telephoneNumber");
		Object displayName = attrs.get("displayName");
		Object name = attrs.get("name");
		Object language = attrs.get("language");
		Object creationDate = attrs.get("whenCreated");
		Object sAMAccountName = attrs.get("sAMAccountName");

		if (noNulls && (sn == null || givenName == null ||
				displayName == null)) {
			return null;
		}
		LdapUser user = new LdapUser();
		user.setUsername(sAMAccountName == null ? null :  (String)((Attribute)sAMAccountName).get());
		user.setLastName(sn == null ? null :  (String)((Attribute)sn).get());
		user.setFirstName(givenName == null ? null : (String)((Attribute)givenName).get());
		user.setEmail(mail == null ? null : (String)((Attribute)mail).get());
		user.setLanguage(language == null ? null : ((String)((Attribute)language).get()).toLowerCase());
		user.setCreationDate(parseLdapDate((String)((Attribute)creationDate).get()));
		if (phone != null) {
			user.setPhone((String)((Attribute)phone).get());
		}
		user.setDisplayName(displayName == null ? null : (String)((Attribute)displayName).get());
		user.setFullName(name == null ? null : (String)((Attribute)name).get());
		return user;
	}
	@Override
	public List<LdapUser> search(String name) {
		List<String> names = null;
		if (!Strings.isNullOrEmpty(name)) {
			names = Arrays.asList(name);
		}
		return search(names);
	}
	@Override
	public List<LdapUser> search(List<String> names) {
		return search(names, 0);
	}
	public List<LdapUser> search(List<String> names, int retriesSoFar) {
		if (Strings.isNullOrEmpty(this.searchBase)) {
			ApplicationException.exception(CommonMessageID.MISSING_CONFIG.getMessage("searchBase"));
		}
		if (Strings.isNullOrEmpty(this.adminUsername)) {
			ApplicationException.exception(CommonMessageID.MISSING_CONFIG.getMessage("adminUsername"));
		}
		if (Strings.isNullOrEmpty(this.adminPwd)) {
			ApplicationException.exception(CommonMessageID.MISSING_CONFIG.getMessage("adminPwd"));
		}

		logger.info("Searching {} in ldap {} ...", names, ldapUrl);
		long t = System.currentTimeMillis();
		DirContext ctx = null;
		NamingEnumeration<?> results = null;
		try {
			ctx = getContext(adminUsername, adminPwd);
			if (ctx == null) {
				return null;
			}

			LinkedList<LdapUser> list = new LinkedList<LdapUser>();

			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			controls.setReturningAttributes(new String[]{
					"sAMAccountName", "sn", "givenName","telephoneNumber",
					"mail", "displayName", "name", "language", "whenCreated"});

			String filter = "(objectclass=user)";
			if (names != null && names.size() > 0) {
				filter = "(&(objectClass=user)(|";
				for (String name : names) {
					filter += String.format("(displayName=*%s*)(mail=*%s*)(mailNickname=*%s*)(sAMAccountName=*%s*)", 
							name, name, name, name);
				}
				filter += "))";
			}
			results = ctx.search(searchBase, filter, controls);

			while (true) {
				try {
					if (results.hasMore()) {
						SearchResult sr = (SearchResult) results.next();
						LdapUser user = retrieveUser(sr.getAttributes(), true);
						if (user != null) {
							list.add(user);
						}
					} else {
						break;
					}
				} catch (PartialResultException e) {
					//OK no more result
					break;
				}
			}
			return list;

		} catch (Exception e) {
			if (retriesSoFar < 2) {
				return search(names, retriesSoFar+1);
			} else {
				logger.error("Failed to search in LDAP!", e);
			}
		} finally {
			logger.info("Search done in ldap {}. Took {} ms", ldapUrl, System.currentTimeMillis() -t);
			if (results != null) {
				try {
					results.close();
				} catch (NamingException e) {
					// Never mind this.
				}
			}
			releaseContext(ctx);
		}
		return null;
	}

	public void setSearchBase(String searchBase) {
		this.searchBase = searchBase;
	}

	public void setAdminUsername(String adminUsername) {
		this.adminUsername = adminUsername;
	}

	public void setAdminPwd(String adminPwd) {
		this.adminPwd = adminPwd;
	}

	protected String parseLdapDate(String ldapDate) {
		/*
		 * 20110105181550.000000Z
		 * LDAP format is a restricted version of one of the date-time formats defined by ISO 8601. 
		 * This style using a minimum of separators is known as “basic” in ISO 8601.
		 * In these formats, the Z on the end is short for Zulu and means UTC (basically same as GMT).
		 * The decimal point and digit at the end represents a fraction of a second.
		 */
		String date = null;
		if (ldapDate != null) {
			try {
				logger.debug("Parsing date atribute {} ...", ldapDate);
				if (!Strings.isNullOrEmpty(ldapDate) && ldapDate.length() >= 14) {
					date = ldapDate.substring(0,4) +"-" +ldapDate.substring(4,6) +"-" +
						ldapDate.substring(6,8) +" " +ldapDate.substring(8,10) +":" +
						ldapDate.substring(10,12) +":" +ldapDate.substring(12,14);
				}
			} catch (Exception e) {
				logger.warn("Failed to parse ldap date {}!", ldapDate);
			}
		}
		return date;
	}
}

/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CriteriaSanitizer<T> {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * You may override this method to confer a different handling per type / other criteria
	 * 
	 * If the method return false, the default behavior will be executed.
	 * 
	 * The default behavior is to convert empty strings to null when empty.
	 * The other parameters are not touched
	 * 
	 * If the method returns true, the default behavior will NOT be executed.
	 *  
	 * @param criteria : the criteria object
	 * @param fieldName : the field name 
	 * @param fieldType : the field type
	 * @param fieldValue : the field value
	 * @param writeMethod : The write method (in order to modify the parameter)
	 * @return true if modification occured and that the default action does not need to be executed
	 */
	protected boolean cleanField(T criteria, String fieldName, Class<?> fieldType, Object fieldValue, Method writeMethod) {
		return false;
		
	}
	/**
	 * The default behavior is to convert empty strings to null when empty.
	 * The other parameters are not touched
	 *  
	 * @param criteria : the criteria object
	 * @param fieldName : the field name 
	 * @param fieldType : the field type
	 * @param fieldValue : the field value
	 * @param writeMethod : The write method (in order to modify the parameter)
	 */
	protected void defaultCleanField(T criteria, String fieldName, Class<?> fieldType, Object fieldValue, Method writeMethod) {
		if (String.class.equals(fieldType)) {
			String newValue = Strings.emptyToNull((String)fieldValue);
			try {
				writeMethod.invoke(criteria, newValue);
			} catch (Exception e){
				logger.warn("Unable to write value from class {}, field {}, cause : {}",new Object [] { fieldType, fieldName,e});
				
			}
		}
	}
	/**
	 * Sanitize the criteria object in parameter.
	 *  
	 * @param criteria the criteria object to be sanitized
	 */
	public void clean(T criteria) {
		Preconditions.checkNotNull(criteria, "Criteria object is mandatory");
		try {
			BeanInfo bi = Introspector.getBeanInfo(criteria.getClass());
			PropertyDescriptor[] pds = bi.getPropertyDescriptors();
			for (PropertyDescriptor pd : pds) {
				String name = pd.getName();
				if ("class".equals(name)) {
					continue;
				}
				Class<?> type =  pd.getPropertyType();
				Object value=null;
				try {
					value = pd.getReadMethod().invoke(criteria);
				} catch (Exception e) {
					logger.warn("Unable to read value from class {}, field {}, cause : {}",new Object [] {type, name,e});
				}
				if (!cleanField(criteria, name, type , value,pd.getWriteMethod())) {
					defaultCleanField(criteria, name, type, value,pd.getWriteMethod());
				}
			}
		} catch (IntrospectionException e) {
			logger.error("Unable to introspect bean! class : {}, object : {}, exception : {}",new Object [] {criteria.getClass(),criteria, e});
		}
	}

}

/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * This object provide simple tools for date manipulation.
 * No need to instatiate this object. all methods are static.
 * 
 * @author Aiman Sawan
 */
public class DateUtils {
	/**
	 * Number of milliseconds per day
	 */
	public static final int MS_PER_DAY = 86400000;
	/**
	 * Number of milliseconds per hour
	 */
	public static final int MS_PER_HOUR = 3600000;
	/**
	 * Number of milliseconds per minute
	 */
	public static final int MS_PER_MINUTE = 60000;
	/**
	 * Number of milliseconds per second
	 */
	public static final int MS_PER_SECOND = 1000;

	/**
	 * Normal date format "yyyy/MM/dd HH:mm:ss".
	 */
	public static final String DATE_FORMAT_NORMAL = "yyyy/MM/dd HH:mm:ss";
	/**
	 * Normal date format "yyyy-MM-dd HH:mm:ss".
	 */
	public static final String DATE_FORMAT_NORMAL_DASH = "yyyy-MM-dd HH:mm:ss";
	/**
	 * Normal full date format "yyyy/MM/dd HH:mm:ss SSS". 
	 */
	public static final String DATE_FORMAT_FULL = "yyyy/MM/dd HH:mm:ss SSS";
	/**
	 * Normal full date format "yyyy-MM-dd HH:mm:ss SSS". 
	 */
	public static final String DATE_FORMAT_FULL_DASH = "yyyy-MM-dd HH:mm:ss SSS";
	/**
	 * Compact full date time format "yyyyMMddHHmmss". 
	 */
	public static final String DATE_FORMAT_COMPACT = "yyyyMMddHHmmss";
	/**
	 * Time format "HH:mm:ss". 
	 */
	public static final String DATE_FORMAT_HHMMSS = "HH:mm:ss";
	/**
	 * Time format "HH:mm". 
	 */
	public static final String DATE_FORMAT_HHMM = "HH:mm";
	/**
	 * Date format "yyyy/MM/dd". 
	 */
	public static final String DATE_FORMAT_YYYYMMDD = "yyyy/MM/dd";
	/**
	 * Date format "yyyy-MM-dd". 
	 */
	public static final String DATE_FORMAT_YYYYMMDD_DASH = "yyyy-MM-dd";
	/**
	 * Date format compact "yyyyMMdd". 
	 */
	public static final String DATE_FORMAT_YYYYMMDD_COMPACT = "yyyyMMdd";

	protected static final String[] DATE_FORMATS =
		{
			DATE_FORMAT_NORMAL,
			DATE_FORMAT_NORMAL_DASH,
			DATE_FORMAT_YYYYMMDD,
			DATE_FORMAT_YYYYMMDD_DASH,
			DATE_FORMAT_COMPACT,
			DATE_FORMAT_YYYYMMDD_COMPACT,
			DATE_FORMAT_HHMMSS };

	/**
	 * 1800/01/01 (no minutes, hours, seconds, ms)
	 * this date is used instead of using null date (not initialized date)
	 */
	public final static Date NULL_MIN_DATE = new Date(-5364644400000L);	
	public final static long NULL_MIN_DATE_DAYS = -5364644400000L / MS_PER_DAY;	
	
	/**
	 * 2999/12/31 (no minutes, hours, seconds, ms)
	 * this date is used instead of using null date (initialized but inifinite date)
	 */
	public final static Date NULL_MAX_DATE = new Date(32503611600000L);;
	public final static long NULL_MAX_DATE_DAYS = 32503611600000L / MS_PER_DAY;	


	/**
	 * Contstructor. No need to instatiate this object. all methods are static.
	 */
	public DateUtils() {
		super();
	}

	/**
	 * Format a date as HH:mm:ss.
	 * 
	 * @param date Date, date to format.
	 * 
	 * @return String, formated date.
	 */
	public static String formatHHMMSS(Date date) {

		if (date == null) {
			throw new IllegalArgumentException("Got null date");
		}
		SimpleDateFormat hhmmssFormat =
			new SimpleDateFormat(DATE_FORMAT_HHMMSS, Locale.getDefault());
		return hhmmssFormat.format(getRealDate(date));
	}
	
	/**
	 * Format a date as HH:mm.
	 * 
	 * @param date Date, date to format.
	 * 
	 * @return String, formated date.
	 */
	public static String formatHHMM(Date date) {

		if (date == null) {
			throw new IllegalArgumentException("Got null date");
		}
		SimpleDateFormat hhmmFormat =
			new SimpleDateFormat(DATE_FORMAT_HHMM, Locale.getDefault());
		return hhmmFormat.format(getRealDate(date));
	}

	/**
	 * Format a date as yyyy/MM/dd.
	 * 
	 * @param date Date, date to format.
	 * 
	 * @return String, formated date.
	 */
	public static String formatYYYYMMDD(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Got null date");
		}
		SimpleDateFormat yyyymmddFormat =
			new SimpleDateFormat(DATE_FORMAT_YYYYMMDD, Locale.getDefault());
		return yyyymmddFormat.format(getRealDate(date));
	}
    
    private static Date getRealDate(Date date) {
        Date d = null;
        if (date instanceof Timestamp) {
            Timestamp timeStamp = (Timestamp) date;
            long tsValue = timeStamp.getTime();
               tsValue += (timeStamp.getNanos() / 1000000);
            d = new Date(tsValue);
        } else if (date instanceof java.util.Date) {
            d = date; 
        }
        
        return d;        
    }
    
	/**
	 * Format a date as a provided pattern.
	 * 
	 * @param pattern String, formatting pattern.
	 * @param date Date, date to format.
	 * 
	 * @return String, formated date.
	 */
	public static String formatDate(String pattern, Date date) {

		if (date == null) {
			throw new IllegalArgumentException("Got null date");
		}

		if (pattern == null) {
			throw new IllegalArgumentException("Got null pattern");
		}
        
        
		SimpleDateFormat dateFormat =
			new SimpleDateFormat(pattern, Locale.getDefault());

		return dateFormat.format(getRealDate(date));
	}
	/**
	 * Parse date form a string.
	 * 
	 * @param stringDate String, date to format.
	 * 
	 * @return Date, parsed date.
	 * 
	 * @throws ParseException if fail to parse date string
	 */
	public static Date parseDate(String stringDate) throws ParseException {
		if (stringDate == null) {
			throw new ParseException("cant parse null date!", 0);
		}
				
		Date d = null;
		for (int i = 0; i < DATE_FORMATS.length; i++) {
			try {
				d = privateParseDate(DATE_FORMATS[i], stringDate);
				break;
			} catch (ParseException e) {
				d = null;
				continue;
			}
		}

		if (d == null) {
			throw new ParseException(
				"Parsing "
					+ stringDate
					+ " failed because the format is not supported !!!",
				0);
		}

		return d;
	}

	/**
	 * Parse date form a string using provided pattern.
	 * 
	 * @param pattern String, formatting pattern.
	 * @param stringDate String, date to format.
	 * 
	 * @return Date, parsed date.
	 * @throws ParseException if fail to parse date string
	 */
	public static Date parseDate(String pattern, String stringDate)
		throws ParseException {

		if (stringDate == null) {
			throw new ParseException("can't parse null date!", 0);
		}

		if (pattern == null) {
			throw new ParseException("can't parse using null pattern!", 0);
		}

		return privateParseDate(pattern, stringDate);
	}
	/**
	 * Modify a date by adding a date part value.
	 * 
	 * @param date Date, date to modify.
	 * @param part int, Date field. Use Calendar constant fields.
	 * @param value int, value to add.
	 */
	public static void addToDate(Date date, int part, int value) {

		if (date == null) {
			throw new IllegalArgumentException("Got null date");
		}

		long time = date.getTime();
		switch (part) {
			case Calendar.DAY_OF_MONTH :
				time += (MS_PER_DAY * value);
				break;
			case Calendar.HOUR_OF_DAY :
				time += (MS_PER_HOUR * value);
				break;
			case Calendar.MINUTE :
				time += (MS_PER_MINUTE * value);
				break;
			case Calendar.SECOND :
				time += (MS_PER_SECOND * value);
				break;
		}

		date.setTime(time);
	}
	/**
	 * Retreive a date part.
	 * 
	 * @param date Date, date from which to retreive a part.
	 * @param part int, Date field. Use Calendar constant fields.
	 * 
	 * @return int, date part as year, month, ...
	 */
	public static int getDatePart(Date date, int part) {

		if (date == null) {
			throw new IllegalArgumentException("Got null date");
		}

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return calendar.get(part);
	}
	/**
	 * Set a date part.
	 * 
	 * @param date Date, date from which to retreive a part.
	 * @param part int, Date field. Use Calendar constant fields.
	 * @param value int, new date value.
	 */
	public static void setDatePart(Date date, int part, int value) {

		if (date == null) {
			throw new IllegalArgumentException("Got null date");
		}

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(part, value);
		date.setTime(calendar.getTime().getTime());
	}
	/**
	 * Change the HH:MM:SS parts of a date.
	 * 
	 * @param date Date, date in which to change the HH:MM:SS part
	 * @param time Date, date in which to get the HH:MM:SS part
	 */
	public static void setDateHHMMSS_GMT(Date date, Date time) {

		if (date == null) {
			throw new IllegalArgumentException("Got null date");
		}

		if (time == null) {
			throw new IllegalArgumentException("Got null time");
		}

		// strip HHMMSS part of the date
		long d = date.getTime() / MS_PER_DAY;

		// extract HHMMSS part of the time
		long t = time.getTime() % MS_PER_DAY;

		date.setTime((d * MS_PER_DAY) + t);
	}
	/**
	 * Reset the HH:MM:SS parts of a date.
	 * 
	 * @param date Date, date in which to zeros the HH:MM:SS parts
	 */
	public static void resetDateHHMMSS_GMT(Date date) {

		if (date == null) {
			throw new IllegalArgumentException("Got null date");
		}

		// strip HHMMSS parts of the date
		long d = date.getTime() / MS_PER_DAY;

		date.setTime(d * MS_PER_DAY);
	}
	/**
	 * Reset the yyyyMMdd parts of a date.
	 * 
	 * @param date Date, date in which to zeros the yyyyMMdd parts
	 */
	public static void resetDateYYYYMMDD_GMT(Date date) {

		if (date == null) {
			throw new IllegalArgumentException("Got null date");
		}

		// extract HHMMSS part of the time
		long t = date.getTime() % MS_PER_DAY;

		date.setTime(t);
	}

	public static void main(String[] args) throws ParseException {
		Date d = parseDate("2002/08/13");
		System.out.println(d);

		d = parseDate("2002-08-13");
		System.out.println(d);

		d = parseDate("2002/08/13 10:25:39");
		System.out.println(d);

		d = parseDate("2002-08-13 10:25:39");
		System.out.println(d);
	}

	public static boolean isNullDate(Date date) {
		return (isNullDateMin(date) || isNullDateMax(date));
	}
	
	public static boolean isNullDateMin(Date date) {
		if (date == null ||
			(date.getTime() / MS_PER_DAY) == NULL_MIN_DATE_DAYS) {
			return true;
		}
		
		return false;
	}
	
	public static boolean isNullDateMax(Date date) {
		// will ignore hours, minutes, seconds, miliseconds.
		if (date == null ||
			(date.getTime() / MS_PER_DAY) == NULL_MAX_DATE_DAYS) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Parse date form a string using provided pattern.
	 * This method assumes argument are ok dans does not log errors.
	 * 
	 * @param pattern String, formatting pattern.
	 * @param stringDate String, date to format.
	 * 
	 * @return Date, parsed date.
	 */
	private static Date privateParseDate(String pattern, String stringDate)
		throws ParseException {

		Date date = null;

		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

		// Don't accept invalid dates such as 1999/15/68
		dateFormat.setLenient(false);

		date = dateFormat.parse(stringDate);

		return date;
	}	
	public Date clearTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	public Date eodTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}
}

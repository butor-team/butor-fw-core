/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package org.butor.mail;

import java.util.Arrays;

/**
 * @author asawan
 *
 */
public class EmailTemplate {
	private String subject;
	private String fromRecipient;
	private String[] toRecipients;
	private String message;
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getFromRecipient() {
		return fromRecipient;
	}
	public void setFromRecipient(String fromRecipient) {
		this.fromRecipient = fromRecipient;
	}
	public String[] getToRecipients() {
		return toRecipients;
	}
	public void setToRecipients(String[] toRecipients) {
		this.toRecipients = toRecipients;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "EmailTemplate [subject=" + subject + ", fromRecipient=" + fromRecipient + ", toRecipients="
				+ Arrays.toString(toRecipients) + ", message=" + message + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fromRecipient == null) ? 0 : fromRecipient.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + Arrays.hashCode(toRecipients);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailTemplate other = (EmailTemplate) obj;
		if (fromRecipient == null) {
			if (other.fromRecipient != null)
				return false;
		} else if (!fromRecipient.equals(other.fromRecipient))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		if (!Arrays.equals(toRecipients, other.toRecipients))
			return false;
		return true;
	}
}

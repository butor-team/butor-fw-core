/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.mail;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

/**
 * @author sawanai
 */
public final class Mailer implements IMailer {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private Queue<Message> queue = new ConcurrentLinkedQueue<Message>();
	private Session session = null;
	private String smtpHost = null;
	private String smtpPort = "0";
	private String smtpUser = null;
	private String smtpPassword = null;
	private InternetAddress fromRecipient = null;
	private ScheduledExecutorService executor = null;

	private boolean ssl = false;

	public Mailer(ScheduledExecutorService executor) throws IllegalArgumentException, AddressException {
		this.executor = executor;

		this.executor.scheduleWithFixedDelay(getMonitor(), 5000, 5000, TimeUnit.MILLISECONDS);
	}

	protected Runnable getMonitor() {
		return new Runnable() {
			@Override
			public void run() {
				try {
					while (true) {
						// will stop when the queue is empty.
						Message message = queue.poll();
						if (message == null)
							break;

						String adrs = "";
						for (Address adr : message.getRecipients(RecipientType.TO)) {
							adrs += adr.toString() + ";";
						}
						logger.info("sending mail to {}, subject={}", adrs, message.getSubject());
						// Send our mail message
						Transport.send(message);
					}
				} catch (Exception e) {
					logger.error("Failed", e);
				}
			}
		};
	}

	@Override
	public void sendMail(String toRecipients, String subject, String content, String fromRecipient) {
		sendMail(toRecipients, subject, content, fromRecipient, "text/plain; charset=UTF-8");
	}
	
	@Override
	public void sendMail(String toRecipients, String subject, String content, String fromRecipient, String contentType) {
		if (Strings.isNullOrEmpty(toRecipients)) {
			logger.warn("Cannot send mail!, no recipients specified.");
			return;
		}
		sendMail(Arrays.asList(toRecipients.split("[;,]")), subject, content, fromRecipient, contentType);
	}

	private class SMTPAuthenticator extends javax.mail.Authenticator {
		String user;
		String pwd;

		private SMTPAuthenticator(String user, String pwd) {
			this.user = user;
			this.pwd = pwd;
		}

		@Override
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(user, pwd);
		}
	}

	@Override
	public void sendMail(List<String> to, String subject, String content, String fromRecipient) {
		this.sendMail(to, subject, content, fromRecipient, "text/plain; charset=UTF-8");
	}
	
	@Override
	public void sendMail(List<String> to, String subject, String content, String fromRecipient, String contentType) {
		if (to == null || to.size() == 0) {
			logger.warn("Cannot send mail!, no recipients specified.");
			return;
		}
		try {
			if (Strings.isNullOrEmpty(smtpHost)) {
				throw new IllegalArgumentException("smtp host not set!");
			}
			if (Strings.isNullOrEmpty(smtpPort)) {
				throw new IllegalArgumentException("smtp port not set!");
			}
			if (Strings.isNullOrEmpty(fromRecipient) && this.fromRecipient == null) {
				throw new IllegalArgumentException("Missing from recipient arg!");
			}
			if (session == null) {
				logger.info(String.format("Connecting to smtp host=%s, port=%s, ssl=%s, user=%s, pwd=%s ...", 
						smtpHost, smtpPort, ssl, smtpUser, "*"));
				Properties props = new Properties();
				props.setProperty("mail.smtp.host", smtpHost);
				props.setProperty("mail.smtp.port", smtpPort);

				if (ssl) {
					props.put("mail.smtp.socketFactory.port", smtpPort);
					props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
				}

				if (!Strings.isNullOrEmpty(smtpUser) || !Strings.isNullOrEmpty(smtpPassword)) {
					logger.info("authenticating ...");
					props.put("mail.smtp.auth", "true");
					Authenticator auth = new SMTPAuthenticator(smtpUser, smtpPassword);
					session = Session.getDefaultInstance(props, auth);
				} else {
					logger.info("Not authenticating cause no user/pwd provided!");
					session = Session.getDefaultInstance(props);
				}
			}

			InternetAddress ia = this.fromRecipient;
			if (!Strings.isNullOrEmpty(fromRecipient)) {
				ia = new InternetAddress(fromRecipient);
			}

			// Prepare our mail message
			Message message = new MimeMessage(session);
			message.setFrom(ia);

			InternetAddress dests[] = new InternetAddress[to.size()];
			int i = 0;
			for (String tt : to) {
				dests[i++] = new InternetAddress(tt);
			}

			message.setRecipients(Message.RecipientType.TO, dests);
			message.setSubject(subject);
			message.setContent(content, contentType);

			logger.info("queued mail to {}, subject={}", to.toString(), subject);
			queue.add(message);
		} catch (Exception e) {
			logger.error("Failed", e);
		}
	}

	public String getFromRecipient() {
		if (this.fromRecipient != null)
			return this.fromRecipient.getAddress();
		return null;
	}

	public void setFromRecipient(String fromRecipient) throws AddressException {
		this.fromRecipient = new InternetAddress(fromRecipient);
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}

	public void setSmtpUser(String smtpUser) {
		this.smtpUser = smtpUser;
	}

	public void setSmtpPassword(String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}

	public static void main(String[] args) {
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("id@gmail.com", "pwd");
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("id@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("id@gmail.com"));
			message.setSubject("Testing java.mail ssl");
			message.setText("Test Mail");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public void setSsl(boolean ssl) {
		this.ssl = ssl;
	}
}

/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.config;

import java.io.File;
import java.util.Properties;

import org.butor.utils.PropertiesHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ButorConfig implements Config {
	private Logger _logger = LoggerFactory.getLogger(this.getClass());
	private String _path = null;
	private File _file = null;
	private Properties _props = null;
	private long _lastModified;

	public ButorConfig() {
	}
	public ButorConfig(String path_) {
		setPath(path_);
	}
	private void reload() {
		_logger.info(String.format("Loanding Properties file=%s", _path));
		if (!_file.exists()) {
			_logger.warn(String.format("File does not exists! file=%s", _path));
			return;
		}
		_lastModified = _file.lastModified();
		Properties props = PropertiesHelper.loadProperties(_file);
		if (props == null) {
			_logger.warn(String.format("Failed to load properties file! file=%s", _path));
			return;
		}
		_logger.info(String.format("Loaded properties file! config=[%s]", props));
		_props = props;
	}
	@Override
	public String get(String key_) {
		if (_lastModified < _file.lastModified()) {
			_logger.info("Properties file changed. Reloading ...");
			reload();
		}
		return get(key_, null);
	}
	@Override
	public String get(String key_, String default_) {
		if (_props == null)
			return null;
		return _props.getProperty(key_, default_);
	}

	@Override
	public void set(String key_, String value_) {
		if (_props == null)
			return;
		_props.setProperty(key_, value_);
	}
	@Override
	public void save() {
		if (_props == null)
			return;
		PropertiesHelper.saveProperties(_props, _file);
	}
	public String getPath() {
		return _path;
	}
	public void setPath(String path_) {
		_path = path_;
		_file = new File(_path);
		reload();
	}
	public long getLastModified() {
		return _lastModified;
	}
}

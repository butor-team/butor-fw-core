/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import static org.junit.Assert.*;

import org.butor.utils.CriteriaSanitizer;
import org.junit.Test;

public class TestCriteriaSanitizer {
	
	
	@Test
	public void testCriteriaSanitize() {
		CriteriaSanitizer<SampleCriteria> cs = new CriteriaSanitizer<SampleCriteria>();
		SampleCriteria sc = new SampleCriteria();
		sc.setD(10.0);
		sc.setCri1("");
		cs.clean(sc);
		assertNull(sc.getCri1());
		assertNotNull(sc.getD());
		assertEquals(10.0,sc.getD(),0);
	}

}

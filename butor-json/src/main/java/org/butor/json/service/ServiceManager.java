/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.service;

import java.util.List;

import org.butor.json.JsonServiceRequest;

public interface ServiceManager {
	void registerServices(List<ServiceComponent> components_);
	void registerService(ServiceComponent component_);
	void unregisterService(ServiceComponent component_);
	void invoke(Context ctx_);
	boolean isBinary(JsonServiceRequest req);
}

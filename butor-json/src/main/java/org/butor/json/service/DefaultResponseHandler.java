/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.service;

import java.util.List;

import com.google.common.collect.Lists;

import org.butor.utils.Message;

public class DefaultResponseHandler<T> implements ResponseHandler<T> {
	private final List<Message> messages = Lists.newArrayList();
	private final List<T> rows = Lists.newArrayList();;
	private final Class<T> responseType;
	
	public DefaultResponseHandler(Class<T> responseType) {
		this.responseType = responseType;
	}

	@Override
	public boolean addMessage(Message msg) {
		messages.add(msg);
		return true;
	}

	@Override
	public boolean addRow(T row) {
		rows.add(row);
		return true;
	}

	@Override
	public void end() {
	}

	public List<Message> getMessages() {
		return messages;
	}

	public List<T> getRows() {
		return rows;
	}
	public T getRow() {
		if (rows != null && rows.size() > 0) {
			return rows.get(0);
		}
		return null;
	}

	@Override
	public Class<T> getResponseType() {
		return responseType;
	}
}

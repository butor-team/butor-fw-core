/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json;

public class JsonResponseHeader {
	private String _reqId;
	private final String _type = Constants.RESPONSE_HEADER_CHUNK;

	public JsonResponseHeader() {
	}
	public JsonResponseHeader(String reqId_) {
		_reqId = reqId_;
	}
	public String getType() {
		return _type;
	}

	public String getReqId() {
		return _reqId;
	}
	public void setReqId(String reqId_) {
		_reqId = reqId_;
	}
	@Override
	public String toString() {
		return String.format("reqId=%s", _reqId);
	}
}

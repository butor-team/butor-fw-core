/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;

import org.butor.json.service.Context;
import org.butor.json.service.DefaultResponseHandler;
import org.butor.json.service.ResponseHandler;
import org.butor.json.util.ContextBuilder;
import org.butor.utils.Message;
import org.junit.Test;

public class TestJsonStreamHandler {
	
	private final static  byte[] RESPONSE = ((char)0+"{'_type':\"_response_header_chunk_\"}"+(char)0+"{username:'test',id:1}"+(char)0).getBytes(Charsets.UTF_8);
	private final static  JsonStreamHandler<TestUser> JSH = new JsonStreamHandler<TestJsonStreamHandler.TestUser>();

	public class TestUser {
		String username;
		int id;
	}

	@Test
	public void testGeneric() throws Exception{
		ByteArrayInputStream bais = new ByteArrayInputStream(RESPONSE);
		final List<TestUser> lists = Lists.newArrayList();
		
		ResponseHandler<TestUser> rspHanler  = new ResponseHandler<TestJsonStreamHandler.TestUser>() {
			@Override
			public void end() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public boolean addRow(TestUser row_) {
				lists.add(row_);
				//don't know what to return. missing javadoc. 
				return false;
			}
			
			@Override
			public boolean addMessage(Message message_) {
				//don't know what to return. missing javadoc. 
				return false;
			}

			@Override
			public Class<TestUser> getResponseType() {
				return TestUser.class;
			}
		};
		
	 
		JSH.parse(bais, rspHanler, "???dontknowwhatisthis parameter???");
		assertEquals(1,lists.size());
		TestUser tu = lists.get(0);
		assertEquals("test",tu.username);
		assertEquals(1,tu.id);
		
		lists.clear();
		
		
	}
	
	@Test
	public void testDefaultResponseHandler() throws Exception {
		ByteArrayInputStream bais = new ByteArrayInputStream(RESPONSE);
		bais = new ByteArrayInputStream(RESPONSE);
		DefaultResponseHandler<TestUser> drh = new DefaultResponseHandler<TestUser>(TestUser.class); 
		JSH.parse(bais, drh, "???dontknowwhatisthis parameter???");
		assertEquals(1,drh.getRows().size());
		TestUser tu = drh.getRows().get(0);
		assertEquals("test",tu.username);
		assertEquals(1,tu.id);
	}
	
	@Test
	public void testContextBuilder() {
		DefaultResponseHandler<String> responseHandler = new DefaultResponseHandler<String>(String.class);
		Context<String> ctx = new ContextBuilder<String>().createCommonRequestArgs("test", "fr").setResponseHandler(responseHandler).build();
		testService(ctx,"toto");
		assertEquals("test",ctx.getRequest().getUserId());
		assertNotNull(ctx.getRequest().getSessionId());
		assertNotNull(ctx.getRequest().getReqId());
		assertEquals("fr",ctx.getRequest().getLang());
		assertEquals(1,responseHandler.getRows().size());
		assertEquals(1,responseHandler.getMessages().size());
		assertEquals("toto",responseHandler.getRows().get(0));
		assertEquals("msgtoto",responseHandler.getMessages().get(0).getMessage());
	}
	
	private void testService(Context<String> ctx, String myParam) {
		assertNotNull(ctx);
		assertNotNull(myParam);
		ctx.getResponseHandler().addRow(myParam);
		ctx.getResponseHandler().addMessage(new Message("msg"+myParam));
	}


}

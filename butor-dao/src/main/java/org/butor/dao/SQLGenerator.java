/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao;

import java.beans.PropertyDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.BeanUtils;

public class SQLGenerator {

	private final String tableName;
	private final PropertyDescriptor[] descriptors;
	private final String dbName;
	
	public SQLGenerator(String dbName,String tableName,Class<?> beanType) {
		this.dbName = dbName;
		this.tableName = tableName;
		ArrayList<PropertyDescriptor> pdList =  new ArrayList<PropertyDescriptor>(Arrays.asList(BeanUtils.getPropertyDescriptors(beanType)));
		for (Iterator<PropertyDescriptor> it=pdList.iterator();it.hasNext();) {
			PropertyDescriptor pd = it.next();
			if (pd.getName().equals("class")) { // to remove Object.getClass() 
				it.remove();
			}
		}
		this.descriptors = pdList.toArray(new PropertyDescriptor[] {});
		
	}
	
	public String createTable(List<String> primaryKeys) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		pw.printf("DROP TABLE IF EXISTS `%s`.`%s`;\n",dbName,tableName);
		pw.printf("CREATE TABLE `%s`.`%s` (\n",dbName,tableName);
		for (int i=0;i<descriptors.length;i++) {
			PropertyDescriptor pd = descriptors[i];
			pw.printf("\t`%s` ", pd.getName());
			Class<?> propertyType = pd.getPropertyType();
			if (propertyType == Date.class && !pd.getName().equals("stamp")) {
				pw.printf("DATE NOT NULL");
			} else if (propertyType == Date.class) {
				pw.printf("TIMESTAMP NOT NULL");
			} else if (propertyType == String.class) {
				pw.printf("VARCHAR() NOT NULL");
			} else if (propertyType == Double.class || propertyType == double.class) {
				pw.printf("DOUBLE NOT NULL");
			} else if (propertyType == Integer.class || propertyType == int.class) {
				pw.printf("INT NOT NULL");
			} else if (propertyType == Boolean.class || propertyType == boolean.class) {
				pw.printf("BOOLEAN NOT NULL");
			} else {
				pw.printf("VARCHAR() NOT NULL FIXME");
			}
			pw.printf(",\n");
			
			
		}
		pw.printf("\tPRIMARY KEY (");
		for (int i=0;i<primaryKeys.size();i++) {
			String key = primaryKeys.get(i);
			pw.printf("`%s`",key);
			if (i+1 != primaryKeys.size()) {
				pw.printf(",");
			} 
		}		
		pw.println("))");
		pw.println("ENGINE = InnoDB;");
		pw.flush();
		return sw.toString();
	}
	
	public String generateSelect(List<String> whereClauses) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		pw.print("SELECT\n\t");
		for (int i=0;i<descriptors.length;i++) {
			 pw.printf("%s", descriptors[i].getName());
				if (i+1 != descriptors.length) {
					 pw.printf(",\n\t");
				} else {
					pw.println();
				}
		}
		pw.printf("FROM\n\t%s.%s\n",dbName,tableName);
		pw.printf("WHERE\n\t");
		for (int i=0;i<whereClauses.size();i++) {
			String wc = whereClauses.get(i);
			pw.printf("%s",wc);
			if (i+1 != whereClauses.size()) {
				pw.printf("\n");
			}
		}
		pw.println();	
		pw.flush();
		return sw.toString();
	}
	
	public String generateInsert() {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		 pw.printf("INSERT INTO %s.%s\n\t(",dbName,tableName);
		for (int i=0;i<descriptors.length;i++) {
			 pw.printf("%s", descriptors[i].getName());
			if (i+1 != descriptors.length) {
				 pw.printf(",\n\t");
			} else {
				pw.println();
			}
		}
		 pw.printf(")\nVALUES (\n",tableName);
		for (int i=0;i<descriptors.length;i++) {
			 pw.printf("\t:%s", descriptors[i].getName());
			if (i+1 != descriptors.length) {
				 pw.printf(",\n");
			} else {
				pw.println();
			}
		}
		pw.println(")");	
		pw.flush();
		return sw.toString();
	}
	
	public String generateUpdate(List<String> whereClauses) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		pw.printf("UPDATE %s.%s\nSET\n",dbName,tableName);
		for (int i=0;i<descriptors.length;i++) {
			String name =descriptors[i].getName();
			pw.printf("\t%s = :%s", name,name);
			if (i+1 != descriptors.length) {
				pw.printf(",\n");
			} else {
				pw.println();
			}
		}
		pw.printf("WHERE\n\t");
		for (int i=0;i<whereClauses.size();i++) {
			String wc = whereClauses.get(i);
			pw.printf("%s",wc);
			if (i+1 != whereClauses.size()) {
				pw.printf("\n");
			}
		}
		pw.println();	
		pw.flush();
		return sw.toString();
	}
	
	public String generateDelete(List<String> whereClauses) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		pw.printf("DELETE FROM %s.%s\n",dbName,tableName);
		pw.printf("WHERE\n\t");
		for (int i=0;i<whereClauses.size();i++) {
			String wc = whereClauses.get(i);
			pw.printf("%s",wc);
			if (i+1 != whereClauses.size()) {
				pw.printf("\n");
			}
		}
		pw.println();	
		pw.flush();
		return sw.toString();
	}
}

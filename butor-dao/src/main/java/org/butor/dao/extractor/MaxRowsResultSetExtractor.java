/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao.extractor;


public class MaxRowsResultSetExtractor<T> extends FilterResultSetExtractor<T> {

	private int _startRow = 0;
	private int _maxRow = -1;

	public MaxRowsResultSetExtractor(Class<T> resultSetClass_) {
		this(resultSetClass_, 0, -1); //get all rows
	}
	public MaxRowsResultSetExtractor(Class<T> resultSetClass_, int startRow_, int maxRow_) {
		super(resultSetClass_);
		_startRow = startRow_;
		_maxRow = maxRow_;
	}
	@Override
	public boolean doStart(int index_) {
		return index_ >= _startRow;
	}
	@Override
	public boolean doStop(int index_) {
		return _maxRow > 0 && index_ >= _maxRow;
	}
	@Override
	public boolean accept(Object row_, int index_) {
		return true;
	}
}

/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao;

import org.springframework.dao.DataAccessException;


/**
 * @deprecated use ApplicationException.exception(DAOMessageID...) instead 
 *
 */
@Deprecated
public class DaoException extends DataAccessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8416950824989723372L;

	public DaoException(String message_) {
		super(message_);
	}

	public DaoException(String message_, Throwable cause_) {
		super(message_, cause_);
	}

}

/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao.test;

import java.util.List;

import org.butor.dao.AbstractDao;
import org.butor.dao.DaoException;
import org.butor.dao.test.bean.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientDaoImpl extends AbstractDao implements ClientDao {

	protected static Logger _logger = LoggerFactory.getLogger(ClientDaoImpl.class);

	private String _readSql;
	private String _listSql;
	private String _insertSql;
	private String _updateSql;
	private String _deleteSql;

	@Override
	public Client readClient(String firstName_, String lastName_)
			throws DaoException {

		Client clt = new Client();
		clt.setFirstName(firstName_);
		clt.setLastName(lastName_);
		List<Client> list = queryList("read", getReadSql(), Client.class, clt);
		if (list != null && list.size() > 0)
			return list.get(0);
		return null;
	}
	@Override
	public Client readClient(Long id_)
			throws DaoException {

		Client clt = new Client();
		clt.setId(id_);
		return query("readClinet", getReadSql(), Client.class, clt);
	}
	@Override
	public List<Client> listClient(String lastName_)
			throws DaoException {

		Client clt = new Client();
		clt.setLastName(lastName_);
		return queryList("list", getListSql(), Client.class, clt);
	}
	@Override
	public boolean manageClient(DaOp op_, Client client_)
			throws DaoException {

		switch (op_) {
		case INSERT: {
			Client clt = new Client();
			clt.setFirstName(client_.getFirstName());
			clt.setLastName(client_.getLastName());
			clt.setTweet(client_.getTweet());
			UpdateResult ur =  update("insert", getInserSql(), clt);
			if (ur.numberOfRowAffected > 0) {
				client_.setId(ur.key.longValue());
				return true;
			}
			return false;
		}
		case UPDATE: {
			Client clt = new Client();
			clt.setId(client_.getId());
			clt.setStamp(client_.getStamp());
			clt.setFirstName(client_.getFirstName());
			clt.setLastName(client_.getLastName());
			clt.setTweet(client_.getTweet());
			return update("update", getUpdateSql(), clt).numberOfRowAffected > 0;
		}
		case DELETE: {
			
			Client clt = readClient(client_.getId());
			if (clt != null) {
				return delete("delete", getDeleteSql(), clt).numberOfRowAffected > 0;
			}
		}
		}
		return false;
	}
	public String getReadSql() {
		return _readSql;
	}
	public void setReadSql(String readSql_) {
		_readSql = readSql_;
	}
	public String getListSql() {
		return _listSql;
	}
	public void setListSql(String listSql_) {
		_listSql = listSql_;
	}
	public String getInserSql() {
		return _insertSql;
	}
	public void setInserSql(String insertSql_) {
		_insertSql = insertSql_;
	}
	public String getUpdateSql() {
		return _updateSql;
	}
	public void setUpdateSql(String updateSql_) {
		_updateSql = updateSql_;
	}
	public String getDeleteSql() {
		return _deleteSql;
	}
	public void setDeleteSql(String deleteSql_) {
		_deleteSql = deleteSql_;
	}

}

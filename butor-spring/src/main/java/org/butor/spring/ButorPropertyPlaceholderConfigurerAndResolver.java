/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.spring;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class ButorPropertyPlaceholderConfigurerAndResolver extends
		org.springframework.beans.factory.config.PropertyPlaceholderConfigurer {
	private Map<String, String> propertiesMap = new ConcurrentHashMap<String, String>();
	ConfigurableListableBeanFactory beanFactory;

	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props)
			throws BeansException {
		this.beanFactory = beanFactory;
		super.processProperties(beanFactory, props);
		StringBuilder buf = new StringBuilder("loaded properties :\n");
		for (String pn : props.stringPropertyNames()) {
			buf.append(pn).append("=").append(props.getProperty(pn)).append("\n");
		}
		logger.info(buf.toString());
	}

	public String resolveEmbeddedValue(String str) {
		return beanFactory.resolveEmbeddedValue(str);
	}

	public String getProperty(String name) {
		if (null == name) {
			return null;
		}
		if (!name.startsWith(DEFAULT_PLACEHOLDER_PREFIX)) {
			name = DEFAULT_PLACEHOLDER_PREFIX +name +DEFAULT_PLACEHOLDER_SUFFIX;
		}
		if (propertiesMap.containsKey(name)) {
			return propertiesMap.get(name);
		}
		String value = beanFactory.resolveEmbeddedValue(name);
		propertiesMap.put(name, value);
		return value;
	}
}